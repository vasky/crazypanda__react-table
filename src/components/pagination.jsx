import React from 'react'

export class Pagination extends React.Component{
    constructor(props) {
        super(props);
        this.amountPage = props.amountPage
        this.cells = new Array(this.amountPage).fill(1)
    }

    render() {
        return(
            <div id="pagination">
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        {  this.cells.map((item,index)=>{
                            if( index === this.props.activeIndex){
                                return <li
                                    onClick={()=>{this.props.updateActiveIndex(index)}}
                                    className="page-item active" key={index}>
                                    <a className="page-link" href="#">{ index+1 }</a>
                                </li>
                            }else{
                                return <li onClick={()=>{this.props.updateActiveIndex(index)}} className="page-item" key={index}><a className="page-link" href="#">{ index+1 }</a></li>
                            }
                        }) }
                    </ul>
                </nav>
            </div>
        )
    }
}
