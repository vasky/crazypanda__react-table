import React from 'react'

export class InputFilter extends React.Component{

    render(){
        return(
            <div className="input-filter col-2">
                <div className="input-group mb-5">
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Введите название профессии"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        onInput={(e=>{
                            this.props.filterInputFunc(e)
                        })}
                    />
                </div>
            </div>
        )
    }
}
