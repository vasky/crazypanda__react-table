import './App.css';
import React from 'react';
import {TableComponent} from './components/table.jsx'
import { Pagination } from "./components/pagination";
import { InputFilter } from "./components/filterInput";

class App extends React.Component{
    constructor() {
        super();
        this.state = {
            data: [
                {category: 'Analytics', name:'Junior Data Analyst (games) // 1', any: 'Подробнее', id:0},
                {category: 'Analytics', name:'User Acquisition Manager', any: 'Подробнее', id:1},
                {category: 'Analytics', name:'Marketing Data Analyst', any: 'Подробнее', id:2},
                {category: 'Programming', name:'Junior Front-end разработчик', any: 'Подробнее', id:3},
                {category: 'Programming', name:'Senior Unity 3D Developer', any: 'Подробнее', id:4},
                {category: 'QA', name:'Tech artist (Photoshop), Part Time', any: 'Подробнее', id:5},
                {category: 'Analytics', name:'Junior Data Analyst (games) // 2', any: 'Подробнее', id:6},
            ],
            activeIndex: 0,
            directionSort: true,
            findStatus: false,
            findedData: ''
        }
        this.numberPerPage = 3
        this.amountPage = Math.ceil(this.state.data.length / this.numberPerPage )
    }

    updateActiveIndex =async ( newIndex ) =>{
        await this.setState({
            activeIndex: newIndex
        })
    }

    sortByDescending = (key) => {
        this.setState(prevState => ({
            data: prevState.data.sort((a,b) => {
                if(prevState.directionSort){
                    if( a[key] > b[key] ){
                        return -1
                    }else {
                        return 1
                    }
                }else{
                    if( a[key] > b[key] ){
                        return 1
                    }else {
                        return -1
                    }
                }
            }),
            directionSort: !prevState.directionSort,
        }))
    }

    filterInput = (e) => {
        const inputText = e.target.value
        if( inputText === '' ){
            this.setState({
                findStatus: false
            })
        }else{
            const satisfyedData = []
            this.state.data.forEach( item => {
                if( item.name.toLowerCase().includes(inputText.toLowerCase()) ) {
                    satisfyedData.push(item.id)
                }
            })
            console.log(satisfyedData)
            this.setState({
                findedData: satisfyedData,
                findStatus: true
            })
        }
    }
    render() {
        return (
            <div className="App">
                { this.state.initialData }
                <TableComponent
                    activeIndex = { this.state.activeIndex }
                    numberPerPage = { this.numberPerPage }
                    data={this.state.data}
                    sortByDescendingFunc = { this.sortByDescending }
                    findStatus = { this.state.findStatus }
                    findedData = { this.state.findedData }
                ></TableComponent>
                <Pagination
                    activeIndex = { this.state.activeIndex }
                    amountPage = { this.amountPage }
                    updateActiveIndex = { this.updateActiveIndex }
                ></Pagination>
                <InputFilter filterInputFunc = {this.filterInput}></InputFilter>
            </div>
        );
    }
}

export default App;
