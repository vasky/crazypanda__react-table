import React from 'react'

export class TableComponent extends React.Component{


    render() {
        return(
            <div id="table">
                <table className="table__body table table-striped table-hover">
                        <thead>
                            <tr>
                                <td>Номер строки</td>
                                <td className="sortItem" onClick={ () => { this.props.sortByDescendingFunc('id') }}>Название профессии</td>
                                <td >Категория</td>
                                <td >Дополнительная информация</td>
                            </tr>
                        </thead>
                    <tbody>
                    { this.props.data.map((item, index)=> {
                        if( this.props.activeIndex * this.props.numberPerPage <= index &&
                            this.props.activeIndex * this.props.numberPerPage + this.props.numberPerPage > index || this.props.findStatus){
                            if(!this.props.findStatus){
                                return <tr key={index}>
                                    <td>{item.id +1 }</td>
                                    <td> {item.name} </td>
                                    <td> {item.category} </td>
                                    <td>{item.any} </td>
                                </tr>
                            }else if(this.props.findedData.includes(item.id)){
                                return <tr key={index}>
                                    <td>{item.id +1 }</td>
                                    <td> {item.name} </td>
                                    <td> {item.category} </td>
                                    <td>{item.any} </td>
                                </tr>
                            }

                        }
                        return null
                    }) }
                    </tbody>
                </table>
            </div>
        )
    }
}
